// Testing doc => https://moleculer.services/docs/0.14/testing.html
import { ServiceBroker, Errors, Context } from 'moleculer';
import { TemplateService } from '../../src/services/template.service';
import { describe, expect, beforeAll, afterAll, it, vi } from 'vitest';

describe("Test 'template' service", () => {
  let broker = new ServiceBroker({ logger: false });
  let service = broker.createService(TemplateService);

  beforeAll(() => broker.start());
  afterAll(() => broker.stop());

  describe('Actions...', () => {
    describe("Test 'template.hello' action", async () => {
      it("should return with 'Hello Moleculer'", async () => {
        // call the action
        const result = await broker.call('template.hello');

        // Check the result
        expect(result).toBe('Hello Moleculer');
      });
    });

    describe("Test 'template.welcome' action", async () => {
      it("should return with 'Welcome'", async () => {
        await expect(
          broker.call('template.welcome', { name: 'Adam' })
        ).resolves.toBe('Welcome, Adam');
      });

      it("should emit 'name.welcome' event ", async () => {
        // Spy on context emit function
        vi.spyOn(Context.prototype, 'emit');

        // Call the action
        await broker.call('template.welcome', { name: 'Adam' });

        // Check if the "emit" was called
        expect(Context.prototype.emit).toBeCalledTimes(1);
        expect(Context.prototype.emit).toHaveBeenCalledWith(
          'name.welcome',
          'Adam'
        );
      });

      it('should reject an ValidationError', async () => {
        expect.assertions(1);
        await expect(broker.call('template.welcome')).rejects.toBeInstanceOf(
          Errors.ValidationError
        );
      });
    });
  });

  describe('Events...', () => {
    describe("Test 'user.created' event", async () => {
      it('should call the event handler', async () => {
        // Mock the "sum" method
        service.sum = vi.fn();

        // Call the "helper.sum" handler
        await service.emitLocalEventHandler('user.created', {
          user: 'john',
          a: 5,
          b: 5,
        });
        // Check if "sum" method was called
        expect(service.sum).toBeCalledTimes(1);
        expect(service.sum).toBeCalledWith(5, 5);

        // Restore the "sum" method
        service.sum.mockRestore();
      });
    });
  });

  // describe('Methods...', () => {
  //   describe("Test 'sum' method", () => {
  //     it('should add two numbers', () => {
  //       // Make a direct call of "sum" method
  //       const result = service.sum(1, 2);

  //       expect(result).toEqual(3);
  //     });
  //   });
  // });
});
